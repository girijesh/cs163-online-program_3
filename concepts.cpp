#include "concepts.h"
using namespace std;

table::table(int size)
{
  hashtable_size = size;
  hashtable = new node*[hashtable_size];
  for (int i = 0; i < hashtable_size; i++)
  {
    hashtable[i] = NULL;
  }
}

table::~table() {
  node * current;
  if(!hashtable){
    return;
  }
  for(int i = 0; i < hashtable_size; ++i){
    current = hashtable[i];
    while(current){
      if(current->keyword){
        delete [] current->keyword;
      }
      if(current->description){
        delete [] current->description;
      }
      if(current->aspects){
        delete [] current->aspects;
      }
      if(current->history){
        delete [] current->history;
      }
      if(current->origin){
        delete [] current->origin;
      }
      current = current->next;
      if(hashtable[i]){
        delete hashtable[i];
        hashtable[i] = NULL;
      }
      hashtable[i] = current;
    }
  }
  delete [] hashtable;
}

int table::hash_function(char * key)
{
  int index = 0;
  int length = strlen(key);
  for(int i = 0; i < length; ++i)
  {
    key += key[i];
  }
  index = index % hashtable_size;
  return index;
}

int table::load(){
  char keyword[200];
  char description[200];
  char aspects[200];
  char history[200];
  char origin[200];
  ifstream fi_in;
  fi_in.open("concepts.txt");
  if(!fi_in)
  {
    cout << "Error opening file" << endl;
    return 0;
  }
  fi_in.get(keyword, 200, '|');
  fi_in.ignore(200, '|');
  while(!fi_in.eof())
  {
    fi_in.get(description, 200, '|');
    fi_in.ignore(200, '|');
    fi_in.get(aspects, 200, '|');
    fi_in.ignore(200, '|');
    fi_in.get(history, 200, '|');
    fi_in.ignore(200, '|');
    fi_in.get(origin, 200, '|');
    fi_in.ignore(200, '|');
    fi_in.ignore(200, '\n');
    // call add function to add the concept to the hashtable.
    add(keyword, description, aspects, history, origin);
    fi_in.get(keyword, 200, '|');
    fi_in.ignore(200, '|');
  }

  fi_in.close();
  return 1;
}

int table::display(char keyword[])
{
  int index = hash_function(keyword);
  node * current = hashtable[index];
  while(current != NULL)
  {
    if(strcmp(current->keyword, keyword) == 0)
    {
      cout << "Keyword: " << current->keyword << endl;
      cout << "Description: " << current->description << endl;
      cout << "Aspects: " << current->aspects << endl;
      cout << "History: " << current->history << endl;
      cout << "Origin: " << current->origin << endl;
      return 1;
    }
    current = current->next;
  }
  cout << "Concept not found" << endl;
  return 0;
}

int table::retrieve(char keyword[], table & list)
{
  int index = hash_function(keyword);
  node * current = hashtable[index];
  while(current != NULL)
  {
    if(strcmp(current->keyword, keyword) == 0)
    {
      list.add(current->keyword, current->description, current->aspects, current->history, current->origin);
      return 1;
    }
    current = current->next;
  }
  cout << "Concept not found" << endl;
  return 0;
}