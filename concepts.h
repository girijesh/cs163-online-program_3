#include <iostream>
#include <cctype>
#include <cstring>
#include <fstream>

const int MAX = 100;


struct node // node for our concepts
{
	char * keyword;
	char * description; 
	char * aspects; 
	char * history; 
	char * origin;
	node * next;
};

class table
{
	public:
		table(int size = 7); // constructor initializing the hashtable size to 7.
		~table(); // destructor
		int add(char keyword[], char description[], char aspects[], char history[], char origin[]); // function to add concepts.
		int display(char keyword[]); // function to display certain concepts based on the keyword of interest
		int retrieve(char keyword[], table & list); // function to retrieve and copy a hashtable to another based on keyword of interest.

		int remove (char keyword[]); // function to remove based on keyword of interest.
		int remove (char keyword[], node *& head);

		int display_personality(char keyword[]); // function
		int display_personality(char keyword[], node * head);
		int load(); // function to open the external file and load it's contents into the hashtable.

	private:
		int hash_function(char * key);

		node ** hashtable;
		int hashtable_size;
};

